DROP DATABASE IF EXISTS `office`;
CREATE DATABASE `office`;
USE `office`;

CREATE TABLE `categories` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT
);

CREATE TABLE `location` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `name` VARCHAR(255) NOT NULL,
        `description` TEXT
); 

CREATE TABLE `Subject matter`(
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`name` VARCHAR(255) NOT NULL,
        `category` 	INT NOT NULL,
        `location` INT NOT NULL,
        `description` TEXT,
        FOREIGN KEY (`category`) REFERENCES `categories`(`id`),
        FOREIGN KEY (`location`) REFERENCES `location` (`id`)
);

INSERT INTO `categories`  (`name`, `description`)
VALUES ('CHAIR', 'Chair in use A chair is a piece of furniture with a raised surface supported by legs,'), ('COMPUTER', 'A computer is a device that can be instructed to carry out sequences of arithmetic or logical operations automatically'); 

INSERT INTO `location` (`name`, `description`)
VALUES ('DIRECTOR OFFICE', 'it is room where only director '),('TEACHING', 'it is room where only techers');

INSERT INTO `Subject matter`( `name`, `category`, `location`, `description`)
VALUES ('CHAIR', 1, 1,  'Chair in use A chair is a piece of furniture with a raised surface supported by legs') 
